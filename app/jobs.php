<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;


class jobs extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = "jobs";

    public function scopeWhereFullText($query, $search)
    {
        return $query->whereRaw(array('$text' => array('$search' => $search)));
    }
}
