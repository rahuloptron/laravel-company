<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\state;
use App\category;
use App\city;
use App\subcategory;
use App\qualification;
use App\gender;
use App\salary;
use App\experience;
use App\jobtype;
use App\skills;
use App\jobs;
class selectController extends Controller
{
    public function __construct()
{
$this->middleware('auth');
}

public function myform()
{
$states = state::pluck("state","_id");
$category = category::pluck("category","_id");
$qualification = qualification::pluck("qualification");
$gender = gender::pluck("gender");
$salary = salary::pluck("salary");
$experience = experience::pluck("experience");
$jobtype = jobtype::pluck("jobtype");
$skills = skills::pluck("skill");
$addedskills = jobs::where('_id','59deeb9e25542b11a00040b2')->first();
$showskills =  $addedskills->skill;
return view('job-post',compact('showskills','skills','states','category','qualification','gender','salary','experience','jobtype'));
}
/**
* Get Ajax Request and restun Data
*
* @return \Illuminate\Http\Response
*/
public function myformAjax($_id)
{
$cities = city::where("state_id",$_id)->pluck("city","_id");

    return json_encode($cities);
    
}
public function formAjax($_id)
{
    $subcategory = subcategory::where("category_id",$_id)->pluck("subcategory","_id");
return json_encode($subcategory);
}
}