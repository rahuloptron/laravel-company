<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Input;
//use App\company;
use App\state;
use App\city;
use App\category;
use App\subcategory;
use Auth;
use App\User;
use App\location;
use Image;
use DB;
use Session;
use App\jobs;
use Response;
class copynewController extends Controller
{
    /*
public function __construct()
{
$this->middleware('auth');
}*/
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
$id = Auth::id();
$user = User::where('_id',$id)->get();
$userid = $user[0]->id;

$dataAccount = jobs::where('user_id',$userid)->orderByDesc('updated_at')->paginate(5);
if(count($dataAccount) > 0)
{
return view('account',compact('dataAccount'));
}else{

return view('notFound');
}

}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
//
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function added(Request $request)
{
$data = $_GET['skill'];
$json = preg_replace('/["]/', '' ,$data);
$myArray = explode(',', $json);

$request->session()->put('myArray',$myArray);


}
public function store(Request $request)
{

$skills = $request->session()->get('myArray');
$resume = $request->file('resume');
$filename = $resume->getClientOriginalName();
//$fileextension = time() . '.' . $resume->getClientOriginalName() . '.' . $resume->getClientOriginalExtension();
// $filename = time() . '.' . $resume->getClientOriginalExtension();
// Image::make($avatar)->resize(300, 300)->save( public_path('/uploads/avatars/' . $filename ) );
//$desitinationpath='/uploads/resume/';
$resume->move( public_path('/uploads/resume/'. $filename ) );


//$id = Auth::id();
$id = Auth::id();
$user = User::where('_id',$id)->get();
$userid = $user[0]->id;
$jobs = new jobs;
$cat_id = $request->get('category');
$category_name = category::where('_id',$cat_id)->first();
$category = $category_name->category;
$subcat_id = $request->get('subcategory');
$subcategory_name = subcategory::where('_id',$subcat_id)->first();
$subcategory = $subcategory_name->subcategory;
$city_id = $request->get('city');
$city_name = city::where('_id',$city_id)->first();
$city = $city_name->city;
$state_id = $request->get('state');
$state_name = state::where('_id',$state_id)->first();
$state = $state_name->state;
$jobs->user_id = $userid;
$jobs->resume = $filename;
$jobs->skill = $skills;
$jobs->category = $category;
$jobs->subcategory = $subcategory;
$jobs->jobtitle = $request->jobtitle;
$jobs->qualification = $request->get('qualification');
$jobs->experience = $request->get('experience');
$jobs->salary = $request->get('salary');
$jobs->jobtype = $request->get('jobtype');
$jobs->gender = $request->get('gender');
$jobs->description = $request->description;
$jobs->companyName = $request->companyName;
$jobs->city =  $city;
$jobs->state = $state;
$jobs->address = $request->address;
$jobs->mobile_number = $request->mobileNumber;
$jobs->email = $request->email;
$jobs->about = $request->about;
$jobs->contact_person = $request->contactPerson;
$jobs->website = $request->website;


/* $this->validate($request,['category'=>'required','jobtitle'=>'required','experience'=>'required','state'=>'required',
'mobileNumber'=>'required',
'email'=>'required|email']);*/

$jobs->save();
return redirect('account');
}
/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{

$items =   jobs::where('_id',$id)->first();
$skills = $items->skill;

$cat = category::all();
$loc = location::all();

return view('index',compact('items','cat','loc','skills'));
}
/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
$editdata = jobs::where('_id',$id)->first();
return view('edit',compact('editdata'));

}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
$update = jobs::where('_id',$id)->first();


$update->jobtitle = $request->jobtitle;
$update->website = $request->website;
$update->city = $request->city;
$update->state = $request->state;
$update->category = $request->category;
$update->subcategory = $request->subcategory;
$update->companyName = $request->companyName;
$update->description = $request->description;
$update->jobtype = $request->jobtype;
$update->salary = $request->salary;
$update->qualification = $request->qualification;
$update->experience = $request->experience;
$update->address = $request->address;
$update->mobile_number = $request->mobileNumber;
$update->email = $request->email;
$update->about = $request->about;
$update->contact_person = $request->contactPerson;
$update->save();
return redirect('account');

}
/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{

$user = jobs::where('_id',$id)->first();
$user->delete();

//return redirect('/account');
return redirect('/account')->with('success','Post Deleted Successfully');
}
public function select()
{
$citiDropdown = city::get();
$stateDropdown = state::get();
return view('/insert',compact('citiDropdown','stateDropdown'));
}

public function getDownload($_id)
{
$getdata = jobs::where('_id',$_id)->get();

$getresume = $getdata[0]->resume;

$file= public_path(). "/uploads/resume/" . $getresume;
$headers = array(
'Content-Type: application/pdf',
);
return response()->download($file, $getresume, $headers);
}
}