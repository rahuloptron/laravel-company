<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\company;
use Auth;
class newController extends Controller
{
/*
public function __construct()
{
$this->middleware('auth');
}*/
/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index()
{
$id = Auth::id();

$data = DB::table("users")
->join("companies", "users.id", "=", "companies.user_id")
->where("users.id", "=", $id)
->get();
return view('account',compact('data'));
}
/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create()
{
//
}
/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
$id = Auth::id();
$company = new company;
$company->user_id = $id;
$company->company_name = $request->companyName;
$company->address = $request->address;
$company->address1 = $request->address1;
$company->mobile_number = $request->mobileNumber;
$company->email = $request->email;
$company->about = $request->about;
$company->contact_person = $request->contactPerson;
$company->city_id = $request->get('state');
$company->state_id = $request->get('city');
$company->category_id = $request->get('category');
$company->subcategory_id = $request->get('subcategory');

$this->validate($request,['companyName'=>'required','address'=>'required',
'address1'=>'required','mobileNumber'=>'required',
'email'=>'required|email','about'=>'required',
'contactPerson'=>'required','category'=>'required','subcategory'=>'required','city'=>'required','state'=>'required']);
$company->save();
return redirect('account');
}
/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{

$items = DB::table('companies')->find($id);
$cityID =  $items->city_id;
$stateID =  $items->state_id;
$catID =  $items->category_id;
$subcatID = $items->subcategory_id;
$cities = DB::table("cities")
->join("companies", "cities.id", "=", "companies.city_id")
->where("cities.id", "=", $cityID)
->where('companies.id','=',$id)
->select ("cities.city")
->get();
$states = DB::table('states')
->join('companies','states.id','=','companies.state_id')
->where('states.id', '=',$stateID )
->where('companies.id','=',$id)
->select('states.state')
->get();
$categories = DB::table('category')
->join('companies','category.id','=','companies.category_id')
->where('category.id', '=',$catID )
->where('companies.id','=',$id)
->select('category.category')
->get();
$subcategories = DB::table('subcategory')
->join('companies','subcategory.id','=','companies.subcategory_id')
->where('subcategory.id', '=',$subcatID )
->where('companies.id','=',$id)
->select('subcategory.subcategory')
->get();

return view('index',compact('items','cities','categories','states','subcategories'));
}
/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit($id)
{
$editdata = company::find($id);
return view('edit',compact('editdata'));
}
/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
$update = company::find($id);

$this->validate($request,['companyName'=>'required','address'=>'required',
'address1'=>'required','mobileNumber'=>'required',
'email'=>'required|email','about'=>'required',
'contactPerson'=>'required']);
$update->company_name = $request->companyName;
$update->address = $request->address;
$update->address1 = $request->address1;
$update->mobile_number = $request->mobileNumber;
$update->email = $request->email;
$update->about = $request->about;
$update->contact_person = $request->contactPerson;
$update->save();
return redirect('account');

}
/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
$user = company::find($id);

$user->delete();
return redirect('/account');
}
public function select()
{
$citiDropdown = DB::table('cities')->get();
$stateDropdown = DB::table('states')->get();
return view('/insert',compact('citiDropdown','stateDropdown'));
}
}