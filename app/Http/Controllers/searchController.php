<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
  use App\location;
use App\category;
use App\jobs;
use App\city;
class searchController extends Controller
{

public function search(Request $request)
{
$term = $request->term;
    $items = category::where('category','LIKE','%'.$term.'%')->get();
if(count($items) == 0){
$searchResult[] =  'NO Item Found';
}else{
foreach ($items as $key => $value) {

$searchResult[] = $value->category;
}
}
return $searchResult;
}
public function searchLoc(Request $request)
{
$term = $request->term;
$items = city::where('city','LIKE','%'.$term.'%')->get();
if(count($items) == 0){
$searchLoc[] =  'NO Item Found';
}else{
foreach ($items as $key => $value) {

$searchLoc[] = $value->city;
}
}
return $searchLoc;
}
public function searchkeyword(Request $request)
{
$term = $request->term;
//$keywords = category::where('category','LIKE','%'.$term.'%')->get();
//$keywords =  jobs::raw()->find(array('$text'=>array('$search'=>$term)));
$keywords =  jobs::whereFullText('%'.$term.'%')->get();
if(count($keywords) == 0){
$searchkeyword[] =  'NO Item Found';
}else{
foreach ($keywords as $key => $value) {

$searchkeyword[] = $value->jobtitle;
}
}
return $searchkeyword;
}
public function searchResult($category,$city)
{
      $categorynew = str_replace('-', ' ', $category);
$citynew = str_replace('-', ' ', $city);
$changecategory = ucfirst($categorynew);
$changecity  = ucfirst($citynew);
$categorydata =  category::where('category', '=', $changecategory)->get();
$category = $categorydata[0]->category;
$citydata = city::where('city',"=",$changecity)->get();
$city =  $citydata[0]->city;

$companyData = jobs::where('category',"=",$category)
->where('city',"=",$city)
->get();
if(count($companyData) == 0){
return redirect('/')->with('alert', 'Search Result Not Found! Please Try Again');

}else{
return view('searchResult',compact('companyData'));
}

}
public function searchCategory($category)
{

$categorynew = str_replace('-', ' ', $category);

$changecategory = ucfirst($categorynew);

$categorydata =  category::where('category', '=', $changecategory)->get();
$category = $categorydata[0]->category;

$catData = jobs::where('category',"=",$category)->get();
if(count($catData) == 0){
return redirect('/')->with('alert', 'Search Result Not Found! Please Try Again');

}else{
return view('searchResult1',compact('catData'));
}

}
public function searchLocation($city)
{


$citynew = str_replace('-', ' ', $city);
$changecity  = ucfirst($citynew);

$citydata = city::where('city',"=",$changecity)->get();
$city =  $citydata[0]->city;


$locData = jobs::where('city',"=",$city)
->get();

if(count($locData) == 0){
return redirect('/')->with('alert', 'Search Result Not Found! Please Try Again');

}else{
return view('searchResult2',compact('locData'));
}

}
}