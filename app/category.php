<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class category extends Eloquent
{
    protected $connection = 'mongodb';
    	protected $collection = "category";
}
