<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/download/{_id}', 'copynewController@getDownload');

Route::get('',"indexController@index");
Route::get('/company',"companyController@index");
Route::get('/index/{id}',"copynewController@show");
Route::get('job-post',function(){
return view('job-post');
});
Route::get('skills',"copynewController@added");
Route::post('/index',"copynewController@store");
Route::get('job-post',"copynewController@select");
Route::DELETE('/destroy/{id}',"copynewController@destroy");
Route::get('edit/{id}',"copynewController@edit");
Route::PUT('edit/{id}',"copynewController@update");
Route::get('job-post',array('as'=>'insert','uses'=>'selectController@myform'));
Route::get('job-post/ajax/{id}',array('as'=>'insert.ajax','uses'=>'selectController@myformAjax'));
Route::get('job-post/ajax1/{id}',array('as'=>'insert.ajax1','uses'=>'selectController@formAjax'));
Route::get('/search',"searchController@search");
Route::get('/searchloc',"searchController@searchLoc");
Route::get('/searchkeyword',"searchController@searchkeyword");
Route::get('/searchResult/{category}/{location}',"searchController@searchResult");
Route::get('/searchCategory/{category}',"searchController@searchCategory");
Route::get('/searchLocation/{location}',"searchController@searchLocation");
Route::get('/account',"copynewController@index")->middleware('auth');
Auth::routes();
Route::get('/home', "HomeController@index")->name('home');/*
Route::get('event-registration', 'OrderController@register');
Route::post('payment', 'OrderController@order');*/
Route::get('profile',"UserController@profile");
Route::post('profile',"UserController@update_avatar");