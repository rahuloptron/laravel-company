<!DOCTYPE html>
<!--[if IE 9]>
<html class="ie9">
  <![endif]-->
  <!--[if !IE]><!-->
  <html lang="en">
    <!--<![endif]-->
    <head>
      <meta charset="utf-8">
      <title>WooShop - Premium html5 eCommerce Template</title>
      <meta name="description" content="Premium bootstrap based eCommerce template">
      <!--[if IE]>
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <![endif]-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link rel="stylesheet" href="css/animate.css">
      <link rel="stylesheet" href="css/bootstrap.css">
      <link rel="stylesheet" href="css/jquery.selectbox.css">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="css/themify-icons.css">
      <link rel="stylesheet" href="css/style.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      
    </head>
    <body>
      <div id="wrapper">
        
        <!-- End #mobile-menu -->
        
        <!-- End #mobile-menu-overlay -->
        
        <!-- End #header -->
        <div id="content">
          <div class="container">
            @if ($message = Session::get('success'))
            <div class="alert alert-success">
              <p>{{ $message }}</p>
            </div>
            @endif
            
            
            <a href="{{ url('/') }}">Back</a>
            <ul class="nav navbar-nav navbar-right">
              
              <!-- Authentication Links -->
              @guest
              <li><a href="{{ route('login') }}">Login</a></li>
              <li><a href="{{ route('register') }}">Register</a></li>
              @else
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                  <li>
                    <a href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  </li>
                  
                </ul>
              </li>
              <li><a href="{{ url('/job-post') }}">Job Post</a></li>
              @endguest
            </ul>
            @yield('content')
            
            
            <!-- <input class="form-control pull-right "  type="text" id="searchItem" name="searchItem" placeholder="Search Here..."> -->
            
            
            
          </div>
          
          
          <!-- End .container -->
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                
                <h1>No Added Yet Any Company</h1>
                <h2>If you want please click <a href="{{'/job-post'}}">Here</a></h2>
                
              </div>
              <!-- End .col-md-12 -->
            </div>
            <!-- End .row -->
          </div>
          <!-- End .container -->
          <div class="xlg-margin hidden-xs hidden-sm"></div>
        </div>
        <!-- End #content -->
        
      </div>
      
      <!-- <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
      <script>
      $( function() {
      
      $( "#searchItem" ).autocomplete({
      source: 'http://localhost:8000/search'
      });
      } );
      </script> -->
    </body>
  </html>