<!DOCTYPE html>
    <html>

    <head>
        <meta charset="UTF-8">
        <title>Listing Of Jobs</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
         <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />

         <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>  
     
        <link rel="stylesheet" href="/css/bootstrap.css">
       <link href='http://fonts.googleapis.com/css?family=Signika:600,400,300' rel='stylesheet' type='text/css'>
       <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

            <!-- jQuery library -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

            <link href="/css/sticky-footer-navbar.css" rel="stylesheet">

            <!-- Latest compiled JavaScript -->
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

                <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
    
    tinymce.init({
    
    selector: 'textarea'
    });
    
    </script>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script> 

           
    </head>

    <body ng-app="myApp" ng-controller="MyController">
        <div class="container_full">

            <nav class="navbar navbar-default">
                <div class="container">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar6">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a href="/"><img src="/uploads/logo/jobzylogo.png" alt="jobzylogo" ></a>
                  </div>
                  <div id="navbar6" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        @guest
                                        <li><a href="{{ route('login') }}">Login</a></li>
                                        <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                        <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <img src="/uploads/avatars/{{ Auth::user()->avatar }}" style="width:32px; height:32px; position:absolute; top:10px; left:-20px; border-radius:50%">
                        {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">

                                    <li><a href="{{ url('/profile') }}">Profile</a></li>
                                    <li><a href="{{ url('/account') }}">Manage Ads</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                                <li><a href="{{ url('/job-post') }}">Post Job</a></li>
                            </li>
                        @endguest
                </ul>
              </div>
              <!--/.nav-collapse -->
            </div>
            <!--/.container-fluid -->
          </nav>

          <div class="container">

            
          <div class="row">
    <div class="col-sm-2" ></div>
    <div class="col-sm-8 well">


             @if (count($errors)>0)
              <div class="alert alert-danger">
           @foreach($errors->all() as $error)

                   <li> {{$error}}</li>
              
              @endforeach
              </div>
              @endif



      <form action="/edit/<?php echo $editdata->id; ?>" method="POST" accept-charset="utf-8" >
     {{ csrf_field() }}
     {{method_field('PUT')}}

    <div class="form-group well well-sm">
    <label class="input-desc">Job Title </label>
    <div>

    <input type="text" name="jobtitle" value="<?php echo $editdata->jobtitle; ?>" class="form-control">
    
    </div>
    </div>

    <div class="row">
              <div class="col-sm-6">

                <div class="well well-sm">
                  
                       <div class="form-group">
                        <label>Category</label>
                        <div>
                        <input type="text" name="category" value="<?php echo $editdata->category; ?>" class="form-control" readonly="readonly">
                        </div>
                        </div>

                     <div class="form-group">
                    <label>Subcategory</label>
                    <div>
                    <input type="text" name="subcategory" value="<?php echo $editdata->subcategory; ?>" class="form-control" readonly="readonly">
                    </div>
                    </div>
                   
      <div class="form-group">
    <label>Qualification</label>
    <div>
    <input type="text" name="qualification" value="<?php echo $editdata->qualification; ?>" class="form-control">
    </div>
    </div>
                            
    </div>
    </div>
              
        <div class="col-sm-6">

    <div class="well well-sm">

    <div class="form-group">
    <label>Experience</label>
    <div>
    <input type="text" name="experience" value="<?php echo $editdata->experience; ?>" class="form-control">
    </div>
    </div>


    <div class="form-group">
    <label>Salary</label>
    <div>
    <input type="text" name="salary" value="<?php echo $editdata->salary; ?>" class="form-control">
    </div>
    </div>

 
    <div class="form-group">
    <label>Job Type</label>
    <div>
    <input type="text" name="jobtype" value="<?php echo $editdata->jobtype; ?>" class="form-control">
    </div>
    </div>

   </div>


 </div>

  </div>

  <div class="form-group">
    <label>Description</label>
    <div>
    <textarea name="description" class="form-control"><?php echo $editdata->description; ?></textarea>
    </div>
    </div>

  <div class="row">
              <div class="col-sm-6">

                <div class="well well-sm">
          <div class="form-group">
    <label>Company Name</label>
    <div>
    <input type="text" name="companyName" value="<?php echo $editdata->companyName; ?>" class="form-control">
    </div>
    </div>

    <div class="form-group">
    <label>State</label>
    <div>
    <input type="text" name="state" value="<?php echo $editdata->state; ?>" class="form-control" readonly="readonly">
    </div>
    </div>

    <div class="form-group">
    <label>City</label>
    <div>
    <input type="text" name="city" value="<?php echo $editdata->city; ?>" class="form-control" readonly="readonly">
    </div>
    </div>


    <div class="form-group">
    <label>Address</label>
    <div >
    <input type="text" name="address" value="<?php echo $editdata->address; ?>" class="form-control">
    </div> 
    </div>

                 
    </div>
    </div>
    
        <div class="col-sm-6">

    <div class="well well-sm">
  
  
    <div class="form-group">
    <label>Mobile Number</label>
    <div>
    <input type="number" name="mobileNumber" value="<?php echo $editdata->mobile_number; ?>" class="form-control">
    
    </div> 
    </div> 
    <div class="form-group">
    <label>Email</label>
    <div >
    <input type="text" name="email" value="<?php echo $editdata->email; ?>" class="form-control">
    
    </div> 
    </div>
  


  <div class="form-group">
    <label>Contact Person</label>
    <div>
    <input type="text" name="contactPerson" value="<?php echo $editdata->contact_person; ?>" class="form-control">
    
    </div> 
    </div>


  <div class="form-group">
    <label>Website</label>
    <div>
    <input type="text" name="website" value="<?php echo $editdata->website; ?>" class="form-control">
    </div>
    </div>


  </div>
  </div>
  </div>


    <div class="form-group">
    <label>About</label>
    <div>
    <textarea name="about" class="form-control"><?php echo $editdata->about; ?></textarea>
    
    </div> 
    </div>

  <div class="form-group">

  <input type="submit" id="submit" name="submit" value="Update" ng-disabled="jobform.$invalid" class="btn btn-success center-block">

  </div>

</form>
  </div>



    
  </div>
  <div class="col-sm-2"></div>
      
  </div>
            <footer class="footer">
                        <div class="container">
                        <span class="text-muted">Footer content here.</span>
                      </div>
                    </footer> 
                    
            </div>

    </body>

    </html>