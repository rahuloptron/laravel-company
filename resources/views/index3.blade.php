<!doctype html>
<html lang="en-gb" class="no-js"> <!--<![endif]-->
<head>
    <title>Jobzy.in</title>
    
    <meta charset="utf-8">
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="images/favicon.html">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Open+Sans:400,800,700italic,700,600italic,600,400italic,300italic,300|Roboto:100,300,400,500,700&amp;subset=latin,latin-ext' type='text/css' />
    
    <link rel="stylesheet" href="css/reset.css" type="text/css" />
    <link rel="stylesheet" href="css/reset1.css" type="text/css" />
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <link rel="stylesheet" href="css/style1.css" type="text/css" />
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    <!-- responsive devices styles -->
    <link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
    <link rel="stylesheet" media="screen" href="css/responsive-leyouts1.css" type="text/css" />
    
    
    
    <!-- sticky menu -->
    <link rel="stylesheet" href="js/sticky-menu/core.css">
    
    <!-- REVOLUTION SLIDER -->
    <link rel="stylesheet" type="text/css" href="js/revolutionslider/css/fullwidth.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="js/revolutionslider/rs-plugin/css/settings.css" media="screen" />
    
    
    
    <!-- tweets -->
    <link rel="stylesheet" href="js/testimonials/fadeeffect.css" type="text/css" media="all">
    
    <!-- fancyBox -->
    
    
</head>
<body>
    <div class="site_wrapper">
        
        <!-- HEADER -->
        <header id="header">
            
            <div id="trueHeader">
                
                <div class="wrapper">
                    
                    <div class="container">
                        
                        <!-- Logo -->
                        <div class="one_fourth"><a href="/" id="logo"></a></div>
                        
                        <!-- Menu -->
                        <div class="three_fourth last">
                            
                            <nav id="access" class="access" role="navigation">
                                
                                <div id="menu" class="menu">
                                    
                                    <ul id="tiny">
                                        
                                        @guest
                                        <li><a href="{{ route('login') }}">Login</a></li>
                                        <li><a href="{{ route('register') }}">Register</a></li>
                                        @else
                                        
                                        <li><a href="">{{ Auth::user()->name }}</a>
                                        
                                        <ul>
                                            <li><a href="{{ url('/profile') }}">Profile</a></li>
                                            <li><a href="{{ url('/account') }}">Manage Ads</a></li>
                                            <li>
                                                <a href="{{ route('logout') }}"
                                                    onclick="event.preventDefault();
                                                    document.getElementById('logout-form').submit();">
                                                    Logout
                                                </a>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                                    {{ csrf_field() }}
                                                </form>
                                            </li>
                                        </ul>
                                        
                                    </li>
                                    <li><a href="{{ url('/job-post') }}">Post Job</a></li>
                                    @endguest
                                    
                                    
                                    
                                </ul>
                                
                            </div>
                            
                            </nav><!-- end nav menu -->
                            
                        </div>
                        
                        
                    </div>
                    
                </div>
                
            </div>
            
            </header><!-- end header -->
            
            <div class="clearfix"></div>
            <div id="home">
                
                <div class="parallax_sec1">
                    <div class="container">
                        <div class="two_third">
                            <h1>India’s Easiest Portal for Placements</h1>
                            
                            <h2>Welcome to Jobzy is Leading recruitment consultants providing end-to-end hiring solutions across industries.We are in the business of effectively understanding your manpower requirements, procuring the candidate with the desired profile and building trusting relationships. </h2>
                            
                        </div>
                        <div class="one_third last">
                            
                            <script type="text/javascript" src="//static.mailerlite.com/data/webforms/446663/u0p2r9.js?v3"></script>
                        </div>
                        
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="clearfix mar_top3"></div>
            <div class="four_col_fusection">
                <div class="container">
                    <div class="clearfix mar_top3"></div>
                    <h1>Job Categories</h1>
                    <div class="clearfix divider_line"></div>
                    <div class="one_fourth">
                        
                        <i class="fa fa-bullhorn" style="font-size:48px;color:gray"></i>
                        
                        <h2>Digital Marketing</h2>
                        
                        
                        </div><!-- end section -->
                        <div class="one_fourth">
                            
                            <i class="fa fa-desktop" style="font-size:48px;color:gray"></i>
                            
                            <h2>IT-Software</h2>
                            
                            
                            </div><!-- end section -->
                            <div class="one_fourth ">
                                
                                <i class="fa fa-hdd-o" style="font-size:48px;color:gray"></i>
                                
                                <h2>IT-Hardware</h2>
                                
                                
                                </div><!-- end section -->
                                
                                
                                
                                <div class="one_fourth last">
                                    
                                    <i class="fa fa-headphones" style="font-size:48px;color:gray"></i>
                                    
                                    <h2>Tellecaller/BPO</h2>
                                    
                                    
                                    </div><!-- end section -->
                                    
                                    <div class="clearfix divider_line"></div>
                                    <div class="one_fourth">
                                        <i class="fa fa-handshake-o" style="font-size:48px;color:gray"></i>
                                        
                                        <h2>Sales and Marketing</h2>
                                        
                                        
                                        </div><!-- end section -->
                                        
                                        <div class="one_fourth">
                                            
                                            <i class="fa fa-clock-o" style="font-size:48px;color:gray"></i>
                                            
                                            <h2>Part time jobs</h2>
                                            
                                            
                                            </div><!-- end section -->
                                            
                                            <div class="one_fourth">
                                                
                                                <i class="fa fa-graduation-cap" style="font-size:48px;color:gray"></i>
                                                
                                                <h2>Freshers Jobs</h2>
                                                
                                                </div><!-- end section -->
                                                
                                                <div class="one_fourth last">
                                                    
                                                    <i class="fa fa-users" style="font-size:48px;color:gray"></i>
                                                    
                                                    <h2>Accounting and Finance</h2>
                                                    
                                                    
                                                    </div><!-- end section -->
                                                    </div><!-- close container -->
                                                    </div><!-- end services -->
                                                    <div class="clearfix mar_top7"></div>
                                                    <div class="twitter_feeds">
                                                        
                                                        <div class="container">
                                                            
                                                            <div class="faide_slider">
                                                                
                                                                <div id="slider">
                                                                    
                                                                    
                                                                    <h1>Testimonials</h1>
                                                                    
                                                                    
                                                                    <div id="slide1">
                                                                        
                                                                        
                                                                        
                                                                        It was definitely very very helpful and really great to have this site that makes it easier to hire candidates. Plus support given to the postings of job offers is really kind. Anytime Jobzy is much better than any other. Great to have you guys.
                                                                        
                                                                        <br /><br />
                                                                        
                                                                        <b> Arun Karmokar </b>
                                                                        </div><!-- end slide1 -->
                                                                        
                                                                        <div id="slide2">
                                                                            
                                                                            
                                                                            Jobzy.in has been a great help in fulfilling the recruitment needs of my organization. The jobzy is a perfect example of latest technology in action, designed simplistically, to attract candidates catering to my organizations needs right from age / location / education to even other skills.
                                                                            <br /><br />
                                                                            
                                                                            <b>Anup Sharma </b>
                                                                            
                                                                            </div><!-- end slide2 -->
                                                                            <div id="slide3">
                                                                                
                                                                                
                                                                                Jobzy.in 3 has been a great help in fulfilling the recruitment needs of my organization. The jobzy is a perfect example of latest technology in action, designed simplistically, to attract candidates catering to my organizations needs right from age / location / education to even other skills.
                                                                                <br /><br />
                                                                                
                                                                                <b>Anup Sharma </b>
                                                                                
                                                                                </div><!-- end slide3 -->
                                                                                <div id="slide4">
                                                                                    
                                                                                    
                                                                                    Jobzy.in 4 has been a great help in fulfilling the recruitment needs of my organization. The jobzy is a perfect example of latest technology in action, designed simplistically, to attract candidates catering to my organizations needs right from age / location / education to even other skills.
                                                                                    <br /><br />
                                                                                    
                                                                                    <b>Anup Sharma </b>
                                                                                    
                                                                                    </div><!-- end slide4 -->
                                                                                    
                                                                                </div>
                                                                                
                                                                                <div class="clearfix mar_top3"></div>
                                                                                
                                                                                <ul class="controlls">
                                                                                    
                                                                                    <li><a href="#" class="slide1"></a></li>
                                                                                    
                                                                                    <li><a href="#" class="slide2"></a></li>
                                                                                    <li><a href="#" class="slide3"></a></li>
                                                                                    <li><a href="#" class="slide4"></a></li>
                                                                                    
                                                                                </ul>
                                                                                
                                                                            </div>
                                                                            
                                                                        </div>
                                                                        
                                                                    </div>
                                                                    <div class="features_sec02">
                                                                        <div class="clearfix mar_top7"></div>
                                                                        <h1>Top Placement</h1>
                                                                        <div class="clearfix divider_line"></div>
                                                                        <div class="container">
                                                                            
                                                                            <div class="one_fifth">
                                                                                
                                                                                <img src="/uploads/logo/dmlogo.png" alt="">
                                                                                
                                                                                </div><!-- end section -->
                                                                                
                                                                                <div class="one_fifth">
                                                                                    
                                                                                    <img src="/uploads/logo/kotak.jpg" alt="">
                                                                                    
                                                                                    </div><!-- end section -->
                                                                                    
                                                                                    <div class="one_fifth">
                                                                                        
                                                                                        <img src="/uploads/logo/redhat.jpg" alt="">
                                                                                        
                                                                                        </div><!-- end section -->
                                                                                        <div class="one_fifth">
                                                                                            
                                                                                            <img src="/uploads/logo/image9.jpg" alt="">
                                                                                            
                                                                                            
                                                                                            </div><!-- end section -->
                                                                                            <div class="one_fifth last">
                                                                                                
                                                                                                <img src="/uploads/logo/toyota.jpg" alt="">
                                                                                                
                                                                                                </div><!-- end section -->
                                                                                                
                                                                                                
                                                                                            </div>
                                                                                            
                                                                                            <div class="clearfix mar_top2"></div>
                                                                                            <div class="container">
                                                                                                
                                                                                                <div class="one_fifth">
                                                                                                    
                                                                                                    <img src="/uploads/logo/51342_1_home_small.jpg" alt="">
                                                                                                    
                                                                                                    </div><!-- end section -->
                                                                                                    
                                                                                                    <div class="one_fifth">
                                                                                                        
                                                                                                        <img src="/uploads/logo/ibm.jpg" alt="">
                                                                                                        
                                                                                                        </div><!-- end section -->
                                                                                                        
                                                                                                        <div class="one_fifth">
                                                                                                            
                                                                                                            <img src="/uploads/logo/infosys.jpg" alt="">
                                                                                                            
                                                                                                            </div><!-- end section -->
                                                                                                            <div class="one_fifth">
                                                                                                                
                                                                                                                <img src="/uploads/logo/green.jpg" alt="">
                                                                                                                
                                                                                                                
                                                                                                                </div><!-- end section -->
                                                                                                                <div class="one_fifth last">
                                                                                                                    
                                                                                                                    <img src="/uploads/logo/intel.png" alt="">
                                                                                                                    
                                                                                                                    </div><!-- end section -->
                                                                                                                    
                                                                                                                    
                                                                                                                </div>
                                                                                                                <div class="clearfix mar_top2"></div>
                                                                                                                <div class="container">
                                                                                                                    
                                                                                                                    <div class="one_fifth">
                                                                                                                        
                                                                                                                        <img src="/uploads/logo/nexus.png" alt="">
                                                                                                                        
                                                                                                                        </div><!-- end section -->
                                                                                                                        
                                                                                                                        <div class="one_fifth">
                                                                                                                            
                                                                                                                            <img src="/uploads/logo/polaris.png" alt="">
                                                                                                                            
                                                                                                                            </div><!-- end section -->
                                                                                                                            
                                                                                                                            <div class="one_fifth">
                                                                                                                                
                                                                                                                                <img src="/uploads/logo/accenture.jpg" alt="">
                                                                                                                                
                                                                                                                                </div><!-- end section -->
                                                                                                                                <div class="one_fifth">
                                                                                                                                    
                                                                                                                                    <img src="/uploads/logo/wipro.jpg" alt="">
                                                                                                                                    
                                                                                                                                    
                                                                                                                                    </div><!-- end section -->
                                                                                                                                    <div class="one_fifth last">
                                                                                                                                        
                                                                                                                                        <img src="/uploads/logo/psi.png" alt="">
                                                                                                                                        
                                                                                                                                        </div><!-- end section -->
                                                                                                                                        
                                                                                                                                        <div class="clearfix mar_top1"></div>
                                                                                                                                    </div>
                                                                                                                                    </div><!-- end features section 2 -->
                                                                                                                                    <div class="clearfix"></div>
                                                                                                                                    </div><!-- end services -->
                                                                                                                                    
                                                                                                                                    <div id="contact">
                                                                                                                                        <div class="copyright_info"        >
                                                                                                                                            Copyright © 2017 Jobzy.in. All rights reserved.  <a href="#">Terms of Use</a> | <a href="#">Privacy Policy</a>
                                                                                                                                            
                                                                                                                                            </div><!-- end copyright info -->
                                                                                                                                            </div><!-- end contact -->
                                                                                                                                        </div>
                                                                                                                                        
                                                                                                                                        <!-- ######### JS FILES ######### -->
                                                                                                                                        <!-- get jQuery from the google apis -->
                                                                                                                                        <script type="text/javascript" src="js/universal/jquery.js"></script>
                                                                                                                                        <!-- tweets -->
                                                                                                                                        <script type="text/javascript">//<![CDATA[
                                                                                                                                        $(window).load(function(){
                                                                                                                                        $(".controlls li a").click(function(e) {
                                                                                                                                        e.preventDefault();
                                                                                                                                        var id = $(this).attr('class');
                                                                                                                                        $('#slider div:visible').fadeOut(500, function() {
                                                                                                                                        $('div#' + id).fadeIn();
                                                                                                                                        })
                                                                                                                                        });
                                                                                                                                        });//]]>
                                                                                                                                        </script>
                                                                                                                                        <script type="text/javascript" src="js/mainmenu/ddsmoothmenu.js"></script>
                                                                                                                                        <script type="text/javascript" src="js/mainmenu/jquery-1.7.1.min.js"></script>
                                                                                                                                        <script type="text/javascript" src="js/mainmenu/selectnav.js"></script>
                                                                                                                                        <script type="text/javascript" src="js/mainmenu/scripts.js"></script>
                                                                                                                                        <!-- fancyBox -->
                                                                                                                                    </body>
                                                                                                                                </html>