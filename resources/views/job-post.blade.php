<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Listing Of Jobs</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link href='http://fonts.googleapis.com/css?family=Signika:600,400,300' rel='stylesheet' type='text/css'>
    
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="/css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="/css/bootstrap-multiselect.css">
    <script src="/js/bootstrap-multiselect.js"  type="text/javascript"></script>
    <script type="text/javascript">
    $(function () {
    $('#skills').multiselect({
    includeSelectAllOption: true
    });
    });
    </script>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>
    
    tinymce.init({
    
    selector: 'textarea'
    });
    
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
    
  </head>
  <body ng-app="myApp" ng-controller="MyController">
    <div class="container_full">
      <nav class="navbar navbar-default">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar6">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a href="/"><img src="/uploads/logo/jobzylogo.png" alt="jobzylogo" ></a>
          </div>
          <div id="navbar6" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              @guest
              <li><a href="{{ route('login') }}">Login</a></li>
              <li><a href="{{ route('register') }}">Register</a></li>
              @else
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  <img src="/uploads/avatars/{{ Auth::user()->avatar }}" style="width:32px; height:32px; position:absolute; top:10px; left:-20px; border-radius:50%">
                  {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="{{ url('/profile') }}">Profile</a></li>
                  <li><a href="{{ url('/account') }}">Manage Ads</a></li>
                  <li>
                    <a href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  </li>
                </ul>
                <li><a href="{{ url('/job-post') }}">Post Job</a></li>
              </li>
              @endguest
            </ul>
          </div>
          <!--/.nav-collapse -->
        </div>
        <!--/.container-fluid -->
      </nav>
      <div class="container">
      <div class="row">
        
        <div class="col-sm-2" ></div>
        <div class="col-sm-8 well">
          @if (count($errors)>0)
          <div class="alert alert-danger">
            @foreach($errors->all() as $error)
            <li> {{$error}}</li>
            
            @endforeach
          </div>
          @endif
          <form action="/index" method="POST" id="target" accept-charset="utf-8" name="jobform" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group well well-sm">
              <label class="input-desc">Job Title <span style="color:red" ng-show="jobform.jobtitle.$touched && jobform.jobtitle.$invalid"> Required </span><span style="color:red">*</span></label>
              <div>
                <input type="text" name="jobtitle" value="{{ old('jobtitle') }}" class="form-control" ng-model="jobtitle" placeholder="Eg. Sales executives needed urgently for ..." autocomplete="off" ng-minlength="15" required>
                
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                
                <div class="well well-sm">
                  <div class="form-group">
                    <label class="input-desc">Upload Resume<span style="color:red">*</span></label>
                    <input type="file" name="resume" class="form-control" valid-file required> 
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                  
                  </div>
                  
                  <div class="form-group">
                    <label class="input-desc">Select Category <span style="color:red" ng-show="jobform.category.$touched && jobform.category.$invalid"> Required </span><span style="color:red">*</span></label>
                    <select name="category" class="form-control" ng-model="category" required>
                      <option value="">--- Select Category ---</option>
                      @foreach ($category as $key => $value)
                      <option value="{{$key}}">{{$value}}</option>
                      
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="input-desc">Select SubCategory</label>
                    <select name="subcategory" class="form-control">
                    </select>
                  </div>
                  
                  
                  <div class="form-group">
                    <label class="input-desc">Qualification</label>
                    <div >
                      <select name="qualification" class="form-control" >
                        <option value="">--- Select Qualification ---</option>
                        @foreach ($qualification as $data)
                        <option value="{{$data}}">{{$data}}</option>
                        
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>
              
              <div class="col-sm-6">
                <div class="well well-sm">
                  <div class="form-group" >
                    <label class="input-desc">Add Skills</label>
                    <div >
                      <select id="skills" multiple="multiple" class="form-control">
                        @foreach($skills as $data)
                        <option value="{{$data}}">{{$data}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="input-desc">Work-Experience  <span style="color:red" ng-show="jobform.experience.$touched && jobform.experience.$invalid"> Required </span><span style="color:red">*</span></label>
                    <div>
                      <select name="experience" class="form-control" ng-model="experience" required>
                        <option value="">--- Select Experience ---</option>
                        @foreach ($experience as $data)
                        <option value="{{$data}}">{{$data}}</option>
                        
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="input-desc">Salary Range</label>
                    <div >
                      <select name="salary" class="form-control" >
                        <option value="">--- Select Salary Range ---</option>
                        @foreach ($salary as $data)
                        <option value="{{$data}}">{{$data}}</option>
                        
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="input-desc">Job Type</label>
                    <div>
                      <select name="jobtype" class="form-control" >
                        <option value="">--- Select JobType ---</option>
                        @foreach ($jobtype as $data)
                        <option value="{{$data}}">{{$data}}</option>
                        
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <!-- <div class="form-group">
                    <label class="input-desc">Gender</label>
                    <div>
                      <select name="gender" class="form-control" >
                        <option value="">Select Gender -</option>
                        @foreach ($gender as $data)
                        
                        <option value="{{$data}}">{{$data}}</option>
                        
                        @endforeach
                      </select>
                    </div>
                  </div>
                  -->
                </div>
              </div>
            </div>
            <div class="form-group well well-sm">
              <label class="input-desc">Job Description</label>
              <div>
                <textarea name="description" class="form-control">{{ old('description') }}</textarea>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-6">
                <div class="well well-sm">
                  
                  <div class="form-group">
                    <label class="input-desc">Company Name</label>
                    <div>
                      <input type="text" name="companyName" value="{{ old('companyName') }}" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="input-desc">Select State <span style="color:red" ng-show="jobform.state.$touched && jobform.state.$invalid"> Required </span><span style="color:red">*</span></label>
                    <select name="state" class="form-control" ng-model="state" required>
                      <option value="">--- Select State ---</option>
                      @foreach ($states as $key => $value)
                      <option value="{{$key}}">{{$value}}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="input-desc">Select City:</label>
                    <select name="city" class="form-control">
                    </select>
                  </div>
                  <div class="form-group">
                    <label class="input-desc">Address</label>
                    <div>
                      <input type="text" name="address" value="{{ old('address') }}" class="form-control">
                    </div>
                  </div>
                  
                </div>
              </div>
              
              <div class="col-sm-6">
                <div class="well well-sm">
                  
                  <div class="form-group">
                    <label class="input-desc">Mobile Number <span style="color:red">*</span><span style="color:red" ng-if="jobform.mobileNumber.$error.required && jobform.mobileNumber.$touched">required field</span><span style="color:red" ng-show="jobform.mobileNumber.$error.pattern"> Enter 10 digit number</span></label>
                    <div>
                      <input name="mobileNumber" ng-model="contact" id="c_num" type="number" ng-required="true" ng-pattern="ph_numbr" autocomplete="off" placeholder="Contact Number" class="form-control">
                      
                      
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="input-desc">Email ID<span style="color:red">*</span><span style="color:red" ng-if="jobform.email.$error.required && jobform.email.$touched"> Required field</span><span style="color:red" ng-show="jobform.email.$error.pattern"> Invalid</span></label>
                    <div>
                      <input name="email" ng-model="email" id="eml" type="text" ng-pattern="eml_add" ng-required="true" autocomplete="off" placeholder="Email" class="form-control" >
                      
                      
                      
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="input-desc">Contact Person</label>
                    <div>
                      <input type="text" name="contactPerson" value="{{ old('contactPerson') }}" class="form-control">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="input-desc">Website</label>
                    <div>
                      <input type="text" name="website" value="{{ old('website') }}" class="form-control">
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="form-group well well-sm">
              <label class="input-desc">About Company</label>
              <div>
                <textarea name="about" class="form-control">{{ old('about') }}</textarea>
              </div>
            </div>
            <div class="form-group">
              <input type="submit" id="submit" name="submit" value="Submit" ng-disabled="jobform.$invalid" class="btn btn-success center-block">
            </div>
          </form>
        </div>
      </div>
      <div class="col-sm-2"></div>
      </div>

    <footer class="footer">
      <div class="container">
        <span class="text-muted">Footer content here.</span>
      </div>
    </footer>
    
  </div>
  
  <script type="text/javascript">
  $(document).ready(function() {
  $('select[name="category"]').on('change', function() {
  var categoryID = $(this).val();
  if(categoryID) {
  $.ajax({
  url: '/job-post/ajax1/'+categoryID,
  type: "GET",
  dataType: "json",
  success:function(data) {
  
  $('select[name="subcategory"]').empty();
  $.each(data, function(key, value) {
  $('select[name="subcategory"]').append('<option value="'+ key +'">'+ value +'</option>');
  });
  }
  });
  }else{
  $('select[name="subcategory"]').empty();
  }
  });
  $('select[name="state"]').on('change', function() {
  var stateID = $(this).val();
  if(stateID) {
  $.ajax({
  url: '/job-post/ajax/'+stateID,
  type: "GET",
  dataType: "json",
  success:function(data) {
  
  $('select[name="city"]').empty();
  $.each(data, function(key, value) {
  $('select[name="city"]').append('<option value="'+ key +'">'+ value +'</option>');
  });
  }
  });
  }else{
  $('select[name="city"]').empty();
  }
  });
  
  });
  </script>
  <script src="/js/angular.js"></script>
  <script type="text/javascript">
  $('#target').on('submit', function(e) {
  

  var selected = $("#skills option:selected");
  var message = [];
  selected.each(function () {
  message.push($(this).text());
  });
  requestData = JSON.stringify(message);
  console.log(requestData);
  var request;
  request = $.ajax({
  url: "skills",
  method: "GET",
  dataType: "json",
  data: { skill: requestData }
  }); 
  
  });
  </script>

  <script type="text/javascript">
 $('#target').on('submit', function(e) {
    $('#target').delay(2000).submit();
});
</script>
  
</body>
</html>