<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Listing Of Jobs</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link href='http://fonts.googleapis.com/css?family=Signika:600,400,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="/css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
  </head>
  <body>
    <div class="container_full">
      <nav class="navbar navbar-default">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar6">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a href="/"><img src="/uploads/logo/jobzylogo.png" alt="jobzylogo" ></a>
          </div>
          <div id="navbar6" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              @guest
              <li><a href="{{ route('login') }}">Login</a></li>
              <li><a href="{{ route('register') }}">Register</a></li>
              @else
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  <img src="/uploads/avatars/{{ Auth::user()->avatar }}" style="width:32px; height:32px; position:absolute; top:10px; left:-20px; border-radius:50%">
                  {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="{{ url('/profile') }}">Profile</a></li>
                  <li><a href="{{ url('/account') }}">Manage Ads</a></li>
                  <li>
                    <a href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  </li>
                </ul>
                <li><a href="{{ url('/job-post') }}">Post Job</a></li>
              </li>
              @endguest
            </ul>
          </div>
          <!--/.nav-collapse -->
        </div>
        <!--/.container-fluid -->
      </nav>
      <div class="container-fluid">
        <div class="container">
        <div class="row">
          <div class="col-sm-3">
            <div class="well">
              
              <div class="col col-3">
                <input class="form-control" type="text" id="searchItem" name="searchItem" placeholder="Search Category...">
              </div><br>
              <div class="col col-3">
                <input class="form-control" type="text" id="searchLoc" name="searchLoc" placeholder="Search Location...">
              </div><br>
              <div class="col col-3">
                <button id="search" class="btn btn-info">Search</button>
              </div>
              
            </div>
          </div>
          <div class="col-sm-9 well">
            <h3 class="text-danger"><?php echo $items->jobtitle ?>  </h3>
            <div class="well">
              <div class="row">
                <div class="col-sm-4"><strong class="strong">Category :</strong>
                  <?php  if(($items->category) == '') { ?>
                  <?php   echo "Not Mentioned" ?>
                  <?php } else { ?>
                  <?php echo $items->category; ?>
                  <?php } ?>
                </div>
                <div class="col-sm-4"><strong class="strong">Location : </strong>
                  <?php  if(($items->city) == '') { ?>
                  <?php   echo "Not Mentioned" ?>
                  <?php } else { ?>
                  <?php echo $items->city; ?>
                  <?php } ?>
                </div>
                <div class="col-sm-4"><strong class="strong">Post date : </strong>
                <?php echo $items->created_at->diffForHumans(); ?></div>
              </div>
              <br><br>
              <strong class="strong">Resume: </strong>

               <?php  if(($items->resume) == '') { ?>
              <?php   echo "Not Uploaded" ?>
              <?php } else { ?>
               <a href="/download/{{ $items->_id }}" class="btn btn-large"><i class="fa fa-download" aria-hidden="true"></i> <?php echo $items->resume ?> </a>
              <?php } ?>


             
              <br><br>
              <strong class="strong">Skills: </strong>

               <?php  if(($items->skills) == "") { ?>
              <?php   echo "Not Added" ?>
              <?php } else { ?>

              @foreach($skills as $key => $value)
              {{$loop->first ? '' : ', '}}
              <span class="nice">{{ $value}}</span>
              
              @endforeach

              <?php } ?>
              
              
              <br><br>
              <strong class="strong">SubCategory: </strong>
              <?php  if(($items->subcategory) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              <?php echo $items->subcategory; ?>
              <?php } ?>
              <br><br>
              <strong class="strong">Qualification:  </strong>
              <?php  if(($items->qualification) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              {{$items->qualification}}
              <?php } ?>
              <br><br>
              <strong class="strong">Salary:  </strong>
              <?php  if(($items->salary) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              {{$items->salary}}
              <?php } ?>
              <br><br>
              <strong class="strong">Work-Experience:  </strong>
              <?php  if(($items->experience) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              {{$items->experience}}
              <?php } ?>
              <br><br>
              <strong class="strong">Job Type:  </strong>
              <?php  if(($items->jobtype) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              {{$items->jobtype}}
              <?php } ?>
              <br><br>
              <strong class="strong">Gender:  </strong>
              
              <?php  if(($items->gender) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              {{$items->gender}}
              <?php } ?>
              
              <br><br>
              <strong class="strong">Description:  </strong>
              <?php  if(($items->description) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              {!!$items->description!!}
              <?php } ?>
            </div>
            <div class="well">
              <strong class="strong">Company Name:  </strong>
              
              <?php  if(($items->companyName) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              {{$items->companyName}}
              <?php } ?>
              <br><br>
              <strong class="strong">State:  </strong>
              <?php  if(($items->state) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              <?php echo $items->state; ?>
              <?php } ?>
              <br><br>
              <strong class="strong">Address:</strong>
              
              <?php  if(($items->address) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              {{$items->address}}
              <?php } ?>
              <br><br>
              
              <strong class="strong">About: </strong>
              <?php  if(($items->about) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              {!! $items->about !!}
              <?php } ?>
              <br><br>
              
              
              <strong class="strong">Contact Person:</strong>
              <?php  if(($items->contact_person) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              {{$items->contact_person}}
              <?php } ?>
              <br><br>
              <strong class="strong">Mobile : </strong>
              <?php  if(($items->mobile_number) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              {{$items->mobile_number}}
              <?php } ?>
              <br> <br>
              <strong class="strong">Email:  </strong>
              <?php  if(($items->email) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              {{$items->email}}
              <?php } ?>
              <br><br>
              <strong class="strong">Website:  </strong>
              <?php  if(($items->website) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              {{$items->website}}
              <?php } ?>
              <br><br>
            </div>
          </div>
        </div>
      </div>
      </div>
      
      <footer class="footer">
        <div class="container">
          <span class="text-muted">Footer content here.</span>
        </div>
      </footer>
      
    </div>
    
    
  </body>
</html>