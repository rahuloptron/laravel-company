<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
    <script src="http://demo.expertphp.in/js/jquery.js"></script>
      <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
    
tinymce.init({

    selector: 'textarea'
});

</script>
</head>
<body>

<a href="/account" class="btn btn-info">Back</a>

<div class="container">



<div class="col-lg-4 col-lg-offset-4">
    <form action="/edit/<?php echo $editdata->id; ?>" method="POST" accept-charset="utf-8" >
     {{ csrf_field() }}
     {{method_field('PUT')}}


     <div class="form-group">
    <label>Category</label>
    <div>
    <input type="text" name="companyName" value="<?php echo $category->category ?>" class="form-control">
    
    </div>
    </div>

    <div class="form-group">
    <label>Subcategory</label>
    <div>
    <input type="text" name="companyName" value="<?php echo $subcategory->subcategory ?>" class="form-control">
    
    </div>
    </div>


    
    <div class="form-group">
    <label>Company Name</label>
    <div>
    <input type="text" name="companyName" value="<?php echo $editdata->company_name; ?>" class="form-control">
    
    </div>
    </div>
    <div class="form-group">
    <label>Address</label>
    <div >
    <input type="text" name="address" value="<?php echo $editdata->address; ?>" class="form-control">
    
    </div> 
    </div>
    <div class="form-group">
    <label>Address 1</label>
    <div>
    <input type="text" name="address1" value="<?php echo $editdata->address1; ?>" class="form-control">
    
    </div> 
    </div>
    <div class="form-group">
    <label>Mobile Number</label>
    <div>
    <input type="number" name="mobileNumber" value="<?php echo $editdata->mobile_number; ?>" class="form-control">
    
    </div> 
    </div> 
    <div class="form-group">
    <label>Email</label>
    <div >
    <input type="text" name="email" value="<?php echo $editdata->email; ?>" class="form-control">
    
    </div> 
    </div>
    <div class="form-group">
    <label>Contact Person</label>
    <div>
    <input type="text" name="contactPerson" value="<?php echo $editdata->contact_person; ?>" class="form-control">
    
    </div> 
    </div>
    <div class="form-group">
    <label>About</label>
    <div>
    <textarea name="about" class="form-control"><?php echo $editdata->about; ?></textarea>
    
     
    </div> 
    </div>

    <div class="form-group">
    <label>State</label>
    <div>
    <input type="text" name="companyName" value="<?php echo $states->state ?>" class="form-control">
    
    </div>
    </div>

    <div class="form-group">
    <label>City</label>
    <div>
    <input type="text" name="companyName" value="<?php echo $cities->city ?>" class="form-control">
    
    </div>
    </div>
    
    <input type="submit" id="submit" name="submit" value="Update" class="btn btn-info">


    </form>

    
    </div>



    </div>
@if (count($errors)>0)
      <div class="alert alert-danger">
   @foreach($errors->all() as $error)

           <li> {{$error}}</li>
      
      @endforeach
      </div>
      @endif
    

</body>
</html>