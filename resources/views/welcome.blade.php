<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    
  </head>
  <body style="color:#e7e7e7;">
    
    </style>
    <div class="container-full">
      <nav class="navbar navbar-default">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar6">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a href="/"><img src="/uploads/logo/jobzylogo.png" alt="jobzylogo" ></a>
          </div>
          <div id="navbar6" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              @guest
              <li><a href="{{ route('login') }}">Login</a></li>
              <li><a href="{{ route('register') }}">Register</a></li>
              @else
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  <img src="/uploads/avatars/{{ Auth::user()->avatar }}" style="width:32px; height:32px; position:absolute; top:10px; left:-20px; border-radius:50%">
                  {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="{{ url('/profile') }}">Profile</a></li>
                  <li><a href="{{ url('/account') }}">Manage Ads</a></li>
                  <li>
                    <a href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  </li>
                </ul>
                <li><a href="{{ url('/job-post') }}">Post Job</a></li>
              </li>
              @endguest
            </ul>
          </div>
        </div>
      </nav>
      <div class="container-fluid">
        <br>
        <br>
        <br>
        <br>
        
        <h1 class="text-center">India's Easiest Job Portal <span> JOBZY.IN</span></h1>
        <div class="row">
          <br>
          <br>
          <div class="text-center col-md-4 col-md-offset-4 ">
            <div class="well">
              <div class="form-group" >
                
                <input type="text" class="form-control" id="searchkeyword" name="searchkeyword" placeholder="Search by Keyword...">
              </div>
              
              
              <div class="form-group" >
                
                <input type="text" class="form-control" id="searchItem" name="searchItem" placeholder="Search Category...">
              </div>
              <div class="form-group">
                
                <input class="form-control" type="text" id="searchLoc" name="searchLoc" placeholder="Search Location...">
              </div>
              <button class="btn btn-info active" id="search">Search</button>
              
            </div>
            
            
            
            @if (session('alert'))
            <div class="alert alert-danger">
              {{ session('alert') }}
            </div>
            @endif
          </div>
          
        </div>
        
      </div>
      <footer class="footer">
        <div class="container">
          <span class="text-muted">Footer content here.</span>
        </div>
      </footer>
    </div>
    
    
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    
    <script>
    $(document).ready(function(){
    $(search).click(function(){
    
    var data = document.getElementById("searchItem").value;
    var data1 = document.getElementById("searchLoc").value;
    if((data == 0) && (data1 == 0))
    {
    alert('Please Select One of them');
    }else if((data == 1) || (data1 == 0))
    {
    var param = data.replace(/ /g, "-").toLowerCase();
    window.location = "/searchCategory/" + param;
    
    
    }else if((data == 0) || (data1 == 1))
    {
    var param1 = data1.replace(/ /g, "-").toLowerCase();
    window.location = "/searchLocation/" + param1;
    
    }else{
    var param = data.replace(/ /g, "-").toLowerCase();
    var param1 = data1.replace(/ /g, "-").toLowerCase();
    window.location = "/searchResult/" + param + "/" + param1;
    
    }
    
    });
    });
    </script>
    
    <script>
    $( function() {
    
    $( "#searchItem" ).autocomplete({
    source: 'http://localhost:8000/search'
    });
    $( "#searchLoc" ).autocomplete({
    source: 'http://localhost:8000/searchloc'
    });
    $( "#searchkeyword" ).autocomplete({
    source: 'http://localhost:8000/searchkeyword'
    });
    });
    </script>
  </body>
</html>