<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Listing Of Jobs</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    
    <link rel="stylesheet" href="/css/bootstrap.css">
    <link href='http://fonts.googleapis.com/css?family=Signika:600,400,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link href="/css/sticky-footer-navbar.css" rel="stylesheet">
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    
  </head>
  <body>
    <div class="container_full">
      <nav class="navbar navbar-default">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar6">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a href="/"><img src="/uploads/logo/jobzylogo.png" alt="jobzylogo" ></a>
          </div>
          <div id="navbar6" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
              @guest
              <li><a href="{{ route('login') }}">Login</a></li>
              <li><a href="{{ route('register') }}">Register</a></li>
              @else
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                  <img src="/uploads/avatars/{{ Auth::user()->avatar }}" style="width:32px; height:32px; position:absolute; top:10px; left:-20px; border-radius:50%">
                  {{ Auth::user()->name }} <span class="caret"></span>
                </a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="{{ url('/profile') }}">Profile</a></li>
                  <li><a href="{{ url('/account') }}">Manage Ads</a></li>
                  <li>
                    <a href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                      Logout
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                    </form>
                  </li>
                </ul>
                <li><a href="{{ url('/job-post') }}">Post Job</a></li>
              </li>
              @endguest
            </ul>
          </div>
          <!--/.nav-collapse -->
        </div>
        <!--/.container-fluid -->
      </nav>
      <div class="container-fluid">
        <div class="container">
        <div class="row">
          <div class="col-sm-2">
          </div>
          
          <div class="col-sm-8">
            
            @foreach($dataAccount as $data)
            <div class="well">
              <h3><a href="{{'/index/'.$data->id}}" target="_blank" >{{$data->jobtitle}}</a> </h3>
              <strong class="strong">Company :</strong><?php  if(($data->companyName) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              {{$data->companyName}}
              <?php } ?><br><br>
              
              <div class="row">
                
                <div class="col-sm-4"><strong class="strong">Location :</strong><?php  if(($data->city) == '') { ?>
                  <?php   echo "Not Mentioned" ?>
                  <?php } else { ?>
                  {{$data->city}}
                <?php } ?> </div>
                <div class="col-sm-4"><strong class="strong">Experience :</strong><?php  if(($data->experience) == '') { ?>
                  <?php   echo "Not Mentioned" ?>
                  <?php } else { ?>
                  {{$data->experience}}
                <?php } ?></div>
                <div class="col-sm-4"><strong class="strong">Post date : </strong>{{$data->created_at->diffForHumans()}}
                </div>
              </div><br>
              <div><strong class="strong">Category :</strong><?php  if(($data->category) == '') { ?>
                <?php   echo "Not Mentioned" ?>
                <?php } else { ?>
                {{$data->category}}
              <?php } ?></div><br>
              
              
              <strong >Description : </strong>
              <?php  if(($data->description) == '') { ?>
              <?php   echo "Not Mentioned" ?>
              <?php } else { ?>
              {{ substr(strip_tags($data->description), 0, 200)}}
              {{ strlen(strip_tags($data->description)) > 200 ? "..." : ""}}
              <?php } ?><br><br>
              <div >
                <div class="row" >
                  <div class="col-sm-1"><a href="{{'/index/'.$data->id}}" class="form-group btn btn-primary">View</a></div>
                  <div class="col-sm-1"><a  class="form-group btn btn-info  " href="{{'/edit/'.$data->id}}">Edit</a>
                </div>
                <div class="col-sm-1">
                  <form class="delete"  action="{{'/destroy/'.$data->id}}" method="post">
                    
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    
                    
                    <input class="form-group btn btn-danger" type="submit" value="Delete">
                    
                  </form>
                </div>
              </div>
            </div>
          </div>
          @endforeach
          
          {{ $dataAccount->links() }}
          <div class="col-sm-2">
          </div>
       
        </div>
             </div>
           </div>
    </div>
        <footer class="footer">
          <div class="container">
            <span class="text-muted">Footer content here.</span>
          </div>
        </footer>
        
 </div>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      <script>
      $(".delete").on("submit", function(){
      return confirm("Do you want to delete this item?");
      });
      </script>
      
      
    </body>
  </html>